const ClientError = require('../../exceptions/ClientError');

class albumsHandler {
  constructor(service, validator) {
    // eslint-disable-next-line no-underscore-dangle
    this._service = service;
    // eslint-disable-next-line no-underscore-dangle
    this._validator = validator;

    this.postAlbumHandler = this.postAlbumHandler.bind(this);
    this.getAlbumByIdHandler = this.getAlbumByIdHandler.bind(this);
    this.editAlbumByIdHandler = this.editAlbumByIdHandler.bind(this);
    this.deleteAlbumByIdHandler = this.deleteAlbumByIdHandler.bind(this);
    this.anyHandler = this.anyHandler.bind(this);
  }

  // eslint-disable-next-line consistent-return
  async postAlbumHandler(request, h) {
    try {
      // eslint-disable-next-line no-underscore-dangle
      this._validator.validateAlbumPayload(request.payload);
      const { name, year } = request.payload;
      // eslint-disable-next-line no-underscore-dangle
      const albumId = await this._service.addAlbum({ name, year });
      // eslint-disable-next-line no-underscore-dangle
      // this._service.test({ name, year });

      const response = h.response({
        status: 'success',
        message: 'album berhasil ditambahkan',
        data: {
          albumId,
        },
      });

      response.code(201);
      return response;
    } catch (error) {
      if (error instanceof ClientError) {
        const response = h.response({
          status: 'fail',
          message: error.message,
        });
        response.code(error.statusCode);
        return response;
      }
    }
  }

  async getAlbumByIdHandler(request, h) {
    try {
      // eslint-disable-next-line no-underscore-dangle
      const album = await this._service.getAlbumById(request.params.id);
      const response = h.response({
        status: 'success',
        message: 'Album berhasil ditambahkan',
        data: {
          album,
        },
      });
      response.code(200);
      return response;
    } catch (error) {
      if (error instanceof ClientError) {
        const response = h.response({
          status: 'fail',
          message: error.message,
        });

        response.code(error.statusCode);
        return response;
      }
      const response = h.response({
        status: 'fail',
        message: 'Maaf, terjadi kegagalan pada server kami.',
      });
      response.code(500);
      return response;
    }
  }

  async editAlbumByIdHandler(request, h) {
    try {
      // eslint-disable-next-line no-underscore-dangle
      this._validator.validateAlbumPayload(request.payload);
      // eslint-disable-next-line no-underscore-dangle
      const result = await this._service.editAlbumById(request.params.id, request.payload);
      const response = h.response({
        status: 'success',
        message: 'Album Berhasil di Update',
        data: {
          result,
        },
      });
      response.code(200);
      return response;
    } catch (error) {
      if (error instanceof ClientError) {
        const response = h.response({
          status: 'fail',
          message: error.message,
        });

        response.code(error.statusCode);
        return response;
      }
      const response = h.response({
        status: 'error',
        message: 'Maaf, terjadi kegagalan pada server kami.',
      });
      response.code(500);
      return response;
    }
  }

  async deleteAlbumByIdHandler(request, h) {
    try {
    // eslint-disable-next-line no-underscore-dangle
      await this._service.deleteAlbumById(request.params.id);
      const response = h.response({
        status: 'success',
        message: 'album berhasil dihapus',
      });

      response.code(200);
      return response;
    } catch (error) {
      if (error instanceof ClientError) {
        const response = h.response({
          status: 'fail',
          message: error.message,
        });
        response.code(error.statusCode);
        return response;
      }

      // Server ERROR!
      const response = h.response({
        status: 'error',
        message: 'Maaf, terjadi kegagalan pada server kami.',
      });
      response.code(500);
      return response;
    }
  }

  // eslint-disable-next-line class-methods-use-this
  async anyHandler(request, h) {
    try {
      const response = h.response({
        status: 'error',
      });

      response.code(404);
      return response;
    } catch (error) {
      if (error instanceof ClientError) {
        const response = h.response({
          status: 'fail',
          message: error.message,
        });
        response.code(error.statusCode);
        return response;
      }

      // Server ERROR!
      const response = h.response({
        status: 'error',
        message: 'Maaf, terjadi kegagalan pada server kami.',
      });
      response.code(500);
      return response;
    }
  }
}

module.exports = albumsHandler;
