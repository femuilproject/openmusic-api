const albumsHandler = require('./handler');
const routes = require('./routes');

module.exports = {
  name: 'albums',
  version: '1.0.0',
  register: async (server, { service, validator }) => {
    // eslint-disable-next-line new-cap
    const AlbumsHandler = new albumsHandler(service, validator);
    server.route(routes(AlbumsHandler));
  },
};
