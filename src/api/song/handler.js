const ClientError = require('../../exceptions/ClientError');

class songsHandler {
  constructor(service, validator) {
    // eslint-disable-next-line no-underscore-dangle
    this._service = service;
    // eslint-disable-next-line no-underscore-dangle
    this._validator = validator;

    this.postSongHandler = this.postSongHandler.bind(this);
    this.getSongByidHandler = this.getSongByidHandler.bind(this);
    this.getSongsHandler = this.getSongsHandler.bind(this);
    this.putSongById = this.putSongById.bind(this);
    this.deleteSongByIdHandler = this.deleteSongByIdHandler.bind(this);
  }

  async postSongHandler(request, h) {
    try {
      // eslint-disable-next-line no-underscore-dangle
      this._validator.validateSongPayload(request.payload);
      // eslint-disable-next-line object-curly-newline
      const { title, year, performer, genre, duration, albumId } = request.payload;
      // eslint-disable-next-line no-underscore-dangle
      const song = await this._service.addSong(title, year, performer, genre, duration, albumId);

      const response = h.response({
        status: 'success',
        message: 'lagu berhasil di tambahkan',
        data: {
          songId: song,
        },
      });

      response.code(201);
      return response;
    } catch (error) {
      if (error instanceof ClientError) {
        const response = h.response({
          status: 'fail',
          message: error.message,
        });

        response.code(error.statusCode);
        return response;
      }

      const response = h.response({
        status: 'error',
        message: 'maaf terjadi kegagalan server',
      });

      response.code(500);
      return response;
    }
  }

  async getSongByidHandler(request, h) {
    try {
      // eslint-disable-next-line no-underscore-dangle
      const song = await this._service.getSongById(request.params.id);
      const response = h.response({
        status: 'success',
        message: 'lagu berhasil di ambil',
        data: {
          song,
        },
      });

      response.code(200);
      return response;
    } catch (error) {
      if (error instanceof ClientError) {
        const response = h.response({
          status: 'fail',
          message: error.message,
        });

        response.code(error.statusCode);
        return response;
      }

      const response = h.response({
        status: 'error',
        message: 'maaf, terjadi kesalahan pada server',
      });

      response.code(500);
      return response;
    }
  }

  async getSongsHandler(request, h) {
    try {
      // eslint-disable-next-line no-underscore-dangle
      const songs = await this._service.getSongs();
      const response = h.response({
        status: 'success',
        message: 'data berhasil di ambil',
        data: {
          songs,
        },
      });

      response.code(200);
      return response;
    } catch (error) {
      if (error instanceof ClientError) {
        const response = h.response({
          status: 'fail',
          message: error.message,
        });

        response.code(error.statusCode);
        return response;
      }

      const response = h.response({
        status: 'fail',
        message: 'maaf, terjadi kesalahan pada server',
      });

      response.code(500);
      return response;
    }
  }

  async putSongById(request, h) {
    try {
      // eslint-disable-next-line no-underscore-dangle
      this._validator.validateSongPayload(request.payload);
      // eslint-disable-next-line no-underscore-dangle
      const song_ = await this._service.editSong(request.params.id, request.payload);
      const response = h.response({
        status: 'success',
        message: 'update data berhasil',
        data: {
          songId: song_,
        },
      });

      response.code(200);
      return response;
    } catch (error) {
      if (error instanceof ClientError) {
        const response = h.response({
          status: 'fail',
          message: error.message,
        });

        response.code(error.statusCode);
        return response;
      }

      const response = h.response({
        status: 'fail',
        message: ' terjadi kesalahan pada server',
      });

      response.code(500);
      return response;
    }
  }

  async deleteSongByIdHandler(request, h) {
    try {
    // eslint-disable-next-line no-underscore-dangle
      await this._service.deleteSongById(request.params.id);
      const response = h.response({
        status: 'success',
        message: 'lagu berhasil dihapus',
      });

      response.code(200);
      return response;
    } catch (error) {
      if (error instanceof ClientError) {
        const response = h.response({
          status: 'fail',
          message: error.message,
        });
        response.code(error.statusCode);
        return response;
      }

      // Server ERROR!
      const response = h.response({
        status: 'error',
        message: 'Maaf, terjadi kegagalan pada server kami.',
      });
      response.code(500);

      return response;
    }
  }
}

module.exports = songsHandler;
