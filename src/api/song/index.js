const songsHandler = require('./handler');
const routes = require('./routes');

// eslint-disable-next-line no-unused-expressions
module.exports = {
  name: 'songs',
  version: '1.0.0',
  register: async (server, { service, validator }) => {
    // eslint-disable-next-line new-cap
    const SongsHandler = new songsHandler(service, validator);
    server.route(routes(SongsHandler));
  },
};
