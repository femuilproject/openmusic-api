const routes = (handler) => [
  {
    method: 'POST',
    path: '/songs',
    handler: handler.postSongHandler,
  },

  {
    method: 'GET',
    path: '/songs/{id}',
    handler: handler.getSongByidHandler,
  },

  {
    method: 'GET',
    path: '/songs',
    handler: handler.getSongsHandler,
  },

  {
    method: 'PUT',
    path: '/songs/{id}',
    handler: handler.putSongById,
  },

  {
    method: 'DELETE',
    path: '/songs/{id}',
    handler: handler.deleteSongByIdHandler,
  },

];

module.exports = routes;
