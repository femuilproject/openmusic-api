class ClientError extends Error {
  constructor(messege, statusCode = 400) {
    super(messege);
    this.statusCode = statusCode;
    this.name = 'ClientError';
  }
}

module.exports = ClientError;
