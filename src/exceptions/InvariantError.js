const ClientError = require('./ClientError');

class InvariantError extends ClientError {
  constructor(messege) {
    super(messege);
    this.name = 'InvariantError';
  }
}

module.exports = InvariantError;
