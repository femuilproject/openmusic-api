const ClientError = require('./ClientError');

class NotFoundError extends ClientError {
  constructor(messege) {
    super(messege, 404);
    this.name = 'NotFoundError';
  }
}

module.exports = NotFoundError;
