require('dotenv').config();
const Hapi = require('@hapi/hapi');
const albums = require('./api/album/index');
const songs = require('./api/song');
const AlbumsServices = require('./services/postgres/albumsServices');
const SongsServices = require('./services/postgres/songsServices');
const albumValidator = require('./validator/albums');
const songValidator = require('./validator/songs');

async function init() {
  const albumService = new AlbumsServices();
  const songService = new SongsServices();
  const server = Hapi.server({
    port: process.env.PORT,
    host: process.env.HOST,
    routes: {
      cors: {
        origin: ['*'],
      },
    },
  });

  await server.register({
    plugin: albums,
    options: {
      service: albumService,
      validator: albumValidator,
    },
  });

  await server.register({
    plugin: songs,
    options: {
      service: songService,
      validator: songValidator,
    },
  });

  await server.start();
  console.log(`Server berjalan pada ${server.info.uri}`);
}

init();
