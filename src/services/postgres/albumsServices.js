const { Pool } = require('pg');
const { nanoid } = require('nanoid');
const InvariantError = require('../../exceptions/InvariantError');
// eslint-disable-next-line no-unused-vars
const NotFoundError = require('../../exceptions/NotFoundError');

// eslint-disable-next-line space-before-blocks, no-unused-vars
class AlbumsServices{
  constructor() {
    // eslint-disable-next-line no-underscore-dangle
    this._pool = new Pool();
  }

  async addAlbum({ name, year }) {
    const id = nanoid(16);
    const createdAt = new Date().toISOString();
    const updatedAt = createdAt;

    const query = {
      text: 'INSERT INTO albums VALUES($1, $2, $3, $4, $5) RETURNING *',
      values: [id, name, year, createdAt, updatedAt],
    };

    // eslint-disable-next-line no-underscore-dangle
    const result = await this._pool.query(query);
    if (!result.rows[0].id) {
      throw new InvariantError('album tidak berhasil di tambahkan');
    }
    return result.rows[0].id;
  }

  async getAlbumById(id) {
    const query = {
      text: 'SELECT * FROM albums WHERE id = $1',
      values: [id],
    };

    // eslint-disable-next-line no-underscore-dangle
    const result = await this._pool.query(query);
    if (!result.rows.length) {
      throw new NotFoundError('album tidak di temukan');
    }

    return result.rows[0];
  }

  async editAlbumById(id, payload) {
    const updatedAt = new Date().toISOString();

    const { name, year } = payload;
    const query = {
      text: 'UPDATE albums SET name = $1, year = $2, updated_at = $3 WHERE id = $4 RETURNING *',
      values: [name, year, updatedAt, id],
    };

    // eslint-disable-next-line no-underscore-dangle
    const result = await this._pool.query(query);
    if (!result.rows.length) {
      throw new NotFoundError('Gagal memperbarui album. Id tidak ditemukan');
    }

    return result.rows[0];
  }

  async deleteAlbumById(id) {
    const query = {
      text: 'DELETE FROM albums WHERE id = $1 ',
      values: [id],
    };

    // eslint-disable-next-line no-underscore-dangle
    const result = await this._pool.query(query);
    if (!result.rowCount) {
      throw new NotFoundError('album gagal dihapus. Id tidak ditemukan');
    }
  }
}

module.exports = AlbumsServices;
