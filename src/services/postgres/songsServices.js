const { Pool } = require('pg');
const { nanoid } = require('nanoid');
const InvariantError = require('../../exceptions/InvariantError');
const NotFoundError = require('../../exceptions/NotFoundError');

// eslint-disable-next-line space-before-blocks, no-unused-vars
class SongsServices{
  constructor() {
    // eslint-disable-next-line no-underscore-dangle
    this._pool = new Pool();
  }

  // eslint-disable-next-line class-methods-use-this
  async addSong(title, year, genre, performer, duration, albumId) {
    const id = nanoid(16);
    const createdAt = new Date().toISOString();
    const updatedAt = createdAt;

    const query = {
      text: 'INSERT INTO songs VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9) RETURNING *',
      values: [id, title, year, performer, genre, duration, albumId, createdAt, updatedAt],
    };

    // eslint-disable-next-line no-underscore-dangle
    const result = await this._pool.query(query);
    if (!result.rows[0]) {
      throw new InvariantError('song tidak di berhasli di tambahkan');
    }

    return result.rows[0].id;
  }

  async getSongById(id) {
    const query = {
      text: 'SELECT * FROM songs WHERE id = $1',
      values: [id],
    };

    // eslint-disable-next-line no-underscore-dangle
    const result = await this._pool.query(query);
    if (!result.rows[0]) {
      throw new NotFoundError('lagu tidak di temukan');
    }

    return result.rows[0];
  }

  async getSongs() {
    // eslint-disable-next-line no-underscore-dangle

    const query = {
      text: 'SELECT id, title, performer FROM songs',
      values: [],
    };

    // eslint-disable-next-line no-underscore-dangle
    const result = await this._pool.query(query);
    if (result.rows.length === 0) {
      throw new NotFoundError('lagu tidak di temukan');
    }
    return result.rows;
  }

  async editSong(id, payload) {
    const updatedAt = new Date().toISOString();
    // console.log(payload);
    // eslint-disable-next-line object-curly-newline
    // eslint-disable-next-line object-curly-newline
    const queryCheck = {
      text: 'SELECT * FROM songs WHERE id = $1',
      values: [id],
    };
    // eslint-disable-next-line object-curly-newline
    const { title, year, performer, genre, duration, albumId } = payload;
    const query = {
      text: 'UPDATE songs SET title = $1, year = $3, performer = $4, genre = $5, album_id = $7, duration = $6, updated_at = $7 WHERE id = $2 RETURNING * ',
      values: [title, id, year, performer, genre, duration, albumId, updatedAt],
    };

    // eslint-disable-next-line no-underscore-dangle
    const check = await this._pool.query(queryCheck);

    if (!check.rowCount) {
      throw new NotFoundError('lagu tidak di temukan');
    } else {
      // eslint-disable-next-line no-underscore-dangle
      const result = await this._pool.query(query);
      return result.rows;
    }
  }

  async deleteSongById(id) {
    const query = {
      text: 'DELETE FROM songs WHERE id = $1',
      values: [id],
    };

    // eslint-disable-next-line no-underscore-dangle
    const result = await this._pool.query(query);

    if (!result.rowCount) {
      throw new NotFoundError('lagu gagal dihapus, Id tidak di temukan');
    }
  }
}

module.exports = SongsServices;
