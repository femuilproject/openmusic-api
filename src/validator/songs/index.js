const InvariantError = require('../../exceptions/InvariantError');
const { songsPayloadScheme } = require('./schema');

const songValidator = {
  validateSongPayload: (payload) => {
    const validationResult = songsPayloadScheme.validate(payload);
    if (validationResult.error) {
      throw new InvariantError(validationResult.error.message);
    }
  },
};

module.exports = songValidator;
